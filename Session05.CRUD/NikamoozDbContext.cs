﻿using Microsoft.EntityFrameworkCore;
using Session05.CRUD.Entities;

namespace Session05.CRUD
{
    public class NikamoozDbContext : DbContext
    {
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<Course> Courses { get; set; }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("server=.;database=Nikamooz;integrated security=true;");
        }
    }
}
