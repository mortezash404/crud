﻿using System;
using System.Linq;
using Session05.CRUD.Entities;

namespace Session05.CRUD
{
    public class Program
    {
        static void Main(string[] args)
        {
            // insert
            using (var ctx = new NikamoozDbContext())
            {
                //ctx.Teachers.Add(new Teacher
                //{
                //    Name = "Saeed"
                //});

                ctx.Courses.Add(new Course
                {
                    Name = "sql server",
                    TeacherId = 2
                });

                ctx.SaveChanges();

            }

            // update

            using (var ctx = new NikamoozDbContext())
            {
                //var teacher = ctx.Teachers.Find(1);
                //teacher.Name = "Alireza";
                //ctx.SaveChanges();

                //var course = ctx.Courses.Find(2);
                //course.Name = "my sql";
                //ctx.SaveChanges();

                var course = ctx.Courses.Find(3);
                course.TeacherId = 1;
                ctx.SaveChanges();
            }

            // delete

            using (var ctx = new NikamoozDbContext())
            {
                var course = new Course
                {
                    Id = 2
                };

                ctx.Courses.Remove(course);

                ctx.SaveChanges();
            }

            // select

            using (var ctx = new NikamoozDbContext())
            {
                var teachers = ctx.Teachers.Select(s => new {s.Name, s.Id});
                foreach (var teacher in teachers)
                {
                    Console.WriteLine(teacher);
                }
            }

            
        }
    }
}
